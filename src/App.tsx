import {
  Grid,
  List,
  ListItem,
  ListItemText,
  Divider,
  Paper,
} from "@material-ui/core";
import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

// Each logical "route" has two components, one for
// the sidebar and one for the main area. We want to
// render both of them in different places when the
// path matches the current URL.

// We are going to use this route config in 2
// spots: once for the sidebar and once in the main
// content section. All routes are in the same
// order they would appear in a <Switch>.
const routes = [
  {
    path: "/",
    exact: true,
    sidebar: () => <div>home!</div>,
    main: () => <h2>Home</h2>,
  },
  {
    path: "/lacamento",
    sidebar: () => <div>lacamento!</div>,
    main: () => <h2>lacamento</h2>,
  },
  {
    path: "/foguetes",
    sidebar: () => <div>foguetes!</div>,
    main: () => <h2>foguetes</h2>,
  },
  {
    path: "/pessoas",
    sidebar: () => <div>pessoas!</div>,
    main: () => <h2>pessoas</h2>,
  },
  {
    path: "/novos-lacamentos",
    sidebar: () => <div>lacamentos!</div>,
    main: () => <h2>lacamentos</h2>,
  },
];

export default function App() {
  return (
    <Router>
      <Grid container spacing={2}>
        <Grid item xs={2}>
          <Grid
            container
            justify="flex-start"
            spacing={2}
            style={{ width: "100%", height: window.innerHeight }}
          >
            <List
              style={{ width: "100%", flex: 1, flexDirection: "row" }}
              component="nav"
              aria-label="main mailbox folders"
            >
              <ListItem button component="a" href="/">
                <Switch>
                  {/* {routes.map((route, index) => (
                    <Route
                      key={index}
                      path={route.path}
                      exact={route.exact}
                      children={<route.sidebar />}
                    />
                    ))} */}
                  <ListItemText primary="Home" />
                </Switch>
              </ListItem>
              <Divider />
              <ListItem button component="a" href="lacamento">
                <ListItemText primary="Lançamento" />
              </ListItem>
              <ListItem button component="a" href="foguetes">
                <ListItemText primary="Foguetes" />
              </ListItem>
              <Divider />
              <ListItem button component="a" href="pessoas">
                <ListItemText primary="Pessoas" />
              </ListItem>
              <List
                style={{ flexDirection: "row-reverse" }}
                component="nav"
                aria-label="main mailbox folders"
              >
                <Divider />
                <ListItem button component="a" href="novos-lacamentos">
                  <ListItemText primary="Novos laçamentos" />
                </ListItem>
              </List>
            </List>
          </Grid>
        </Grid>
        <Grid item xs={10}>
          <Paper>
            <Grid container>
              <Grid item>
                <Switch>
                  {routes.map((route, index) => (
                    // Render more <Route>s with the same paths as
                    // above, but different components this time.
                    <Route
                      key={index}
                      path={route.path}
                      exact={route.exact}
                      children={<route.main />}
                    />
                  ))}
                </Switch>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </Router>
  );
}
