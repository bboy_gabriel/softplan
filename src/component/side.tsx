import {
  Divider,
  Grid,
  List,
  ListItem,
  ListItemText,
  Paper,
} from "@material-ui/core";
import React from "react";
import { Route, Switch } from "react-router";
import "./App.css";

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height,
  };
}

function side() {
  return (
    <Grid container spacing={2}>
      <Grid item xs={2}>
        <Grid
          container
          justify="flex-start"
          spacing={2}
          style={{ width: "100%", height: getWindowDimensions().height }}
        >
          <List
            style={{ width: "100%", flex: 1, flexDirection: "row" }}
            component="nav"
            aria-label="main mailbox folders"
          >
            <ListItem button component="a" href="home">
              <ListItemText primary="Titulo" />
            </ListItem>
            <Divider />
            <ListItem button component="a" href="lacamento">
              <ListItemText primary="Lançamento" />
            </ListItem>
            <ListItem button component="a" href="foguetes">
              <ListItemText primary="Foguetes" />
            </ListItem>
            <Divider />
            <ListItem button component="a" href="pessoas">
              <ListItemText primary="Pessoas" />
            </ListItem>
            <List
              style={{ flexDirection: "row-reverse" }}
              component="nav"
              aria-label="main mailbox folders"
            >
              <Divider />
              <ListItem button component="a" href="novos-lacamentos">
                <ListItemText primary="Novos laçamentos" />
              </ListItem>
            </List>
          </List>
        </Grid>
      </Grid>
      <Grid item xs={10}>
        <Paper>
          <Grid container>
            <Grid item>
              <Switch>
                <Route exact path="/">
                  <view>teste2</view>
                </Route>
                <Route path="/about">
                  <view>teste2</view>
                </Route>
                <Route path="/dashboard">
                  <view>teste2</view>
                </Route>
              </Switch>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
}

export default side;
